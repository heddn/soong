# Contributing

Contributions are **welcome** and will be fully **credited**. There's still a lot of refinement to be done to Soong - this is your opportunity to get involved with a new framework (and community) on the ground floor! As mentioned above, the plan is ultimately to break out components into small well-contained libraries - these will be excellent opportunities to get your feet wet maintaining your own open-source project. Mike Ryan will be happy to help mentor new contributors.

There's plenty of work already identified in [the Gitlab issue queue](https://gitlab.com/soongetl/soong/issues). Feel free to browse, ask questions, and offer your own insights - or, if you have a migration itch you'd like to scratch and don't see an existing issue, open a new one.

## Working on issues

1. If you have an issue you'd like to work on, assign it to yourself.
1. If you haven't already, fork the project to your account.
1. Create a feature branch in your fork. Recommended branch name is `<gitlab issue #>-<dash-separated-issue-title>`
1. Develop your solution locally. Be sure to:
    - Make sure your changes are fully tested (see below).
    - Make sure your changes are fully documented (see below).
    - Follow the [PSR-2 Coding Standard](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md). Check the code style with `$ composer check-style` - many issues can be automatically fixed with `$ composer fix-style`. The only complaints you should see from `check-style` are long lines in tests.
1. Make sure each individual commit in your pull request is meaningful. If you had to make multiple intermediate commits while developing, please [squash them](http://www.git-scm.com/book/en/v2/Git-Tools-Rewriting-History#Changing-Multiple-Commit-Messages) before submitting.
1. Commits should reference the issue number - e.g., a commit for [Add community docs up front](https://gitlab.com/soongetl/soong/issues/51) might have the commit message "#51: Expand community documentation and move to docs directory.".
1. On gitlab, create a merge request and submit it. 

## Tests
Automated tests are critical, especially when code is changing rapidly. They help ensure that any changes made don't produce any unexpected consequences, and give confidence that a new piece of code does what it's expected to do. In the Soong `tests` directory, you'll find existing tests laid out in parallel with the `src` directory. Of particular note is `tests/Contracts` - while interfaces can't be tested (since they don't do anything to test), we do provide base classes here which you should extend for the tests of your components - these will give you testing that your components meet the documented expectations of the interfaces, so in writing tests you can focus on the specific features added by your own code.

To run the test suite locally:

``` bash
$ composer test
```

## Documentation

* Classes and methods are to be fully documented in comment blocks - these are used to automatically generate the API Reference section of the [online documentation](https://soong-etl.readthedocs.io/).
* Add any non-trivial changes you've made to the [CHANGELOG](CHANGELOG.md).
* Review `README.md` and any `.md` files under `docs` to see if any changes need to be made there.

## Release checklist

Process for tagging a new release:

1. Review the changes going into the release at `https://gitlab.com/soongetl/soong/compare/x.y.z...master` (where `x.y.z` is the previous release).
1. Make sure all significant changes are reflected in [CHANGELOG](CHANGELOG.md).
1. Make sure the documentation is up-to-date with all changes.
1. Run tests and quality reports (TBD, scrutinizer-ci.com has incomplete Gitlab integration) and make sure there are no errors, or regressions in coverage or quality scores.
1. Review all issues labelled for the next release (e.g., `https://gitlab.com/soongetl/soong/issues?label_name%5B%5D=0.7.0`) and triage them: Are there any we should complete first? Any which should be deprioritized? Change the tags on those that remain to the next release.
1. Review all `@todo` tags in the code. Create issues in Github for any which are still relevant, and remove all of them.
1. Add a new heading `## [0.6.0] - 2019-05-01` (replacing `0.6.0` with the new release number and `2019-05-01` with the release date) to [CHANGELOG](CHANGELOG.md) below `[Unreleased]`
1. Add a link for the new release at the bottom of [CHANGELOG](CHANGELOG.md), and update the `[Unreleased]` link to reflect the new release number.
1. Make sure any changes made in the preceding steps are merged into `master`.
1. [Create the new tag](https://gitlab.com/soongetl/soong/-/tags).
