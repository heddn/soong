<?php

namespace Soong\Tests\Transformer\Record;

use Soong\Data\BasicRecord;
use Soong\Tests\Contracts\Transformer\RecordTransformerTestBase;

/**
 * Tests the \Soong\Transformer\Record\Copy class.
 */
class CopyTest extends RecordTransformerTestBase
{

    /**
     * Specify the class we're testing.
     */
    protected function setUp(): void
    {
        $this->transformerClass = '\Soong\Transformer\Record\Copy';
    }

    /**
     * Test copying of various records.
     *
     * @return array
     */
    public function transformerDataProvider(): array
    {
        /* @todo Mock Records */
        return [
            'complete copy, empty previous' => [
                [],
                new BasicRecord(
                    ['source1' => 'foo', 'source2' => 'bar']
                ),
                new BasicRecord([]),
                new BasicRecord(
                    ['source1' => 'foo', 'source2' => 'bar']
                ),
            ],
            'complete copy, overlapping previous' => [
                [],
                new BasicRecord(
                    ['source1' => 'foo', 'source2' => 'bar']
                ),
                new BasicRecord(['source1' => 'other foo']),
                new BasicRecord(
                    ['source1' => 'foo', 'source2' => 'bar']
                ),
            ],
            'complete copy, unique previous' => [
                [],
                new BasicRecord(
                    ['source1' => 'foo', 'source2' => 'bar']
                ),
                new BasicRecord(['source3' => 'other foo']),
                new BasicRecord(
                    [
                        'source1' => 'foo',
                        'source2' => 'bar',
                        'source3' => 'other foo',
                    ]
                ),
            ],
            'include, empty previous' => [
                ['include' => ['source1']],
                new BasicRecord(
                    ['source1' => 'foo', 'source2' => 'bar']
                ),
                new BasicRecord([]),
                new BasicRecord(
                    ['source1' => 'foo']
                ),
            ],
            'include, overlapping previous' => [
                ['include' => ['source1']],
                new BasicRecord(
                    ['source1' => 'foo', 'source2' => 'bar']
                ),
                new BasicRecord(['source1' => 'other foo']),
                new BasicRecord(
                    ['source1' => 'foo']
                ),
            ],
            'include, unique previous' => [
                ['include' => ['source1']],
                new BasicRecord(
                    ['source1' => 'foo', 'source2' => 'bar']
                ),
                new BasicRecord(['source3' => 'other foo']),
                new BasicRecord(
                    [
                        'source1' => 'foo',
                        'source3' => 'other foo',
                    ]
                ),
            ],
            'exclude, empty previous' => [
                ['exclude' => ['source1']],
                new BasicRecord(
                    ['source1' => 'foo', 'source2' => 'bar']
                ),
                new BasicRecord([]),
                new BasicRecord(
                    ['source2' => 'bar']
                ),
            ],
            'exclude, overlapping previous' => [
                ['exclude' => ['source1']],
                new BasicRecord(
                    ['source1' => 'foo', 'source2' => 'bar']
                ),
                new BasicRecord(['source1' => 'other foo']),
                new BasicRecord(
                    ['source1' => 'other foo', 'source2' => 'bar']
                ),
            ],
            'exclude, unique previous' => [
                ['exclude' => ['source1']],
                new BasicRecord(
                    ['source1' => 'foo', 'source2' => 'bar']
                ),
                new BasicRecord(['source3' => 'other foo']),
                new BasicRecord(
                    [
                        'source2' => 'bar',
                        'source3' => 'other foo',
                    ]
                ),
            ],
        ];
    }
}
