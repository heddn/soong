<?php

namespace Soong\Tests\Transformer\Property;

use Soong\Tests\Contracts\Transformer\PropertyTransformerTestBase;

/**
 * Tests the \Soong\Transformer\Property\ValueLookup class.
 */
class ValueLookupTest extends PropertyTransformerTestBase
{

    /**
     * Specify the class we're testing.
     */
    protected function setUp() : void
    {
        $this->transformerClass = '\Soong\Transformer\Property\ValueLookup';
    }

    /**
     * Test looking up various types of values.
     *
     * @return array
     */
    public function transformerDataProvider() : array
    {
        $lookupTable = [
            'lookup_table' => [
                -1 => -5,
                '1' => 'one',
                '0' => 'zero',
                '' => 'empty string',
                'a string' => 'b string',
                '1.2345' => 6.789,
            ]
        ];
        return [
            'negative integer' => [$lookupTable, -1, -5, null],
            'numeric string' => [$lookupTable, '1', 'one', null],
            'zero' => [$lookupTable, 0, 'zero', null],
            'empty string' => [$lookupTable, '', 'empty string', null],
            'non-empty string' => [$lookupTable, 'a string', 'b string', null],
            'float' => [$lookupTable, '1.2345', 6.789, null],
            'array' => [$lookupTable, ['blah'], null,
                'ValueLookup property transformer: expected scalar value, received array'],
            'object' => [$lookupTable, new \stdClass, null,
                'ValueLookup property transformer: expected scalar value, received object'],
            'null' => [$lookupTable, null, null, null],
        ];
    }
}
