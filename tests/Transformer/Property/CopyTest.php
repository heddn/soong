<?php

namespace Soong\Tests\Transformer\Property;

use Soong\Tests\Contracts\Transformer\PropertyTransformerTestBase;

/**
 * Tests the \Soong\Transformer\Property\Copy class.
 */
class CopyTest extends PropertyTransformerTestBase
{

    /**
     * Specify the class we're testing.
     */
    protected function setUp() : void
    {
        $this->transformerClass = '\Soong\Transformer\Property\Copy';
    }

    /**
     * Test copying of various types of values
     *
     * @return array
     */
    public function transformerDataProvider() : array
    {
        return [
            'positive integer' => [[], 1, 1, null],
            'negative integer' => [[], -1, -1, null],
            'numeric string' => [[], '1', '1', null],
            'zero' => [[], 0, 0, null],
            'empty string' => [[], '', '', null],
            'non-empty string' => [[], 'a string', 'a string', null],
            'float' => [[], 1.2345, 1.2345, null],
            'null' => [[], null, null, null],
            'empty array' => [[], [], [], null],
            'non-empty array' => [[], [1, 2, 'five'], [1, 2, 'five'], null],
            'keyed array' => [[], ['a' => 'b', 'c' => 'd'], ['a' => 'b', 'c' => 'd'], null],
            'object' => [[], new \stdClass(), new \stdClass(), null],
            'true' => [[], true, true, null],
            'false' => [[], false, false, null],
        ];
    }
}
