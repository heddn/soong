<?php

namespace Soong\Tests\Task;

use Soong\Tests\Contracts\Task\TaskTestBase;

/**
 * Tests the \Soong\Task\Task class.
 */
class TaskTest extends TaskTestBase
{

    /**
     * @inheritdoc
     */
    protected function setUp() : void
    {
        parent::setUp();
        $this->taskClass = '\Soong\Task\SimpleTask';
    }

    public function testNYI() : void
    {
        $this->markTestIncomplete('No task tests have been implemented yet.');
    }
}
