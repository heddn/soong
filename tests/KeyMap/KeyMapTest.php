<?php
declare(strict_types=1);

namespace Soong\Tests\KeyMap;

use Soong\Tests\Contracts\KeyMap\KeyMapTestBase;

class KeyMapTest extends KeyMapTestBase
{
    /**
     * @inheritdoc
     */
    protected function setUp() : void
    {
        parent::setUp();
        $this->keyMapClass = '\Soong\Tests\KeyMap\TestKeyMap';
    }
}
