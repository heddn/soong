<?php
declare(strict_types=1);

namespace Soong\Tests\KeyMap;

use Soong\KeyMap\KeyMapBase;

/**
 * Implementation of key maps using local memory storage.
 */
class TestKeyMap extends KeyMapBase
{

    /**
     * @var array
     */
    protected $keyMap = [];

    /**
     * @inheritdoc
     */
    public function saveKeyMap(array $extractedKey, array $loadedKey) : void
    {
        $extractorValues = array_values($extractedKey);
        $extractedKey = array_combine(
            array_keys($this->getConfigurationValue('extractor_keys')),
            $extractorValues
        );
        $loadedKey = empty($loadedKey) ? [] : array_combine(
            array_keys($this->getConfigurationValue('loader_keys')),
            array_values($loadedKey)
        );
        $hashedKey = $this->hashKeys($extractorValues);
        $this->keyMap[$hashedKey] = [
            'extracted' => $extractedKey,
            'loaded' => $loadedKey,
        ];
    }

    /**
     * @inheritdoc
     */
    public function lookupLoadedKey(array $extractedKey) : ?array
    {
        $hashedKey = $this->hashKeys(array_values($extractedKey));
        return $this->keyMap[$hashedKey]['loaded'] ?? null;
    }

    /**
     * @inheritdoc
     */
    public function lookupExtractedKeys(array $loadedKey) : array
    {
        $loadedKey = array_values($loadedKey);
        $return = [];
        foreach ($this->keyMap as $contents) {
            if ($loadedKey == array_values($contents['loaded'])) {
                $return[] = $contents['extracted'];
            }
        }
        return $return;
    }

    /**
     * @inheritdoc
     */
    public function delete(array $extractedKey) : void
    {
        $hashedKey = $this->hashKeys(array_values($extractedKey));
        unset($this->keyMap[$hashedKey]);
    }

    /**
     * @inheritdoc
     */
    public function count() : int
    {
        return count($this->keyMap);
    }

    /**
     * @inheritdoc
     */
    public function iterate() : iterable
    {
        foreach ($this->keyMap as $hashedKey => $contents) {
            yield $contents['extracted'];
        }
    }
}
