<?php

namespace Soong\Tests\Contracts\Data;

use PHPUnit\Framework\TestCase;
use Soong\Contracts\Data\RecordFactory;

/**
 * Base class for testing Record implementations.
 *
 * To test a record class, simply extend this class and implement
 * setUp(), assigning the fully-qualified class name to recordFactoryClass:
 *
 * @code
 *     protected function setUp() : void
 *     {
 *         $this->recordFactoryClass = '\Soong\Data\RecordFactory';
 *     }
 * @endcode
 */
abstract class RecordTestBase extends TestCase
{

    /**
     * Fully-qualified name of a RecordFactory implementation.
     *
     * @var RecordFactory $recordFactoryClass
     */
    protected $recordFactoryClass;

    /**
     * Provides record data to test.
     *
     * @return array
     */
    public function recordDataProvider() : array
    {
        $basicData = [
            'string_property' => 'value1',
            'int_property' => 0,
            'null_property' => null,
        ];
        $data['string property'] = [
            $basicData,        // Data for initializing a record.
            'string_property',  // Property to get.
            'value1',           // Expected value of the property.
            true,               // Expected existence of the property.
        ];
        $data['int property'] = [
            $basicData,
            'int_property',
            0,
            true,
        ];
        $data['null property'] = [
            $basicData,
            'null_property',
            null,
            true,
        ];
        $data['missing property'] = [
            $basicData,
            'i_do_not_exist',
            null,
            false,
        ];
        return $data;
    }

    /**
     * Test toArray(), getPropertyValue() and propertyExists().
     *
     * @dataProvider recordDataProvider
     *
     * @param array $data
     *   The data array for initially constructing the record.
     * @param string $propertyName
     *   Name of property to test.
     * @param mixed $expectedPropertyValue
     *   Expected result from getPropertyValue($propertyName).
     * @param bool $expectedPropertyExists
     *   Expected result from propertyExists($propertyName).
     */
    public function testRecord(array $data, string $propertyName, $expectedPropertyValue, bool $expectedPropertyExists)
    {
        /** @var RecordFactory $factory */
        $factory = new $this->recordFactoryClass();
        $record = $factory->create($data);
        $this->assertEquals($data, $record->toArray(), 'Record retrieved');
        $this->assertEquals(
            $expectedPropertyValue,
            $record->getPropertyValue($propertyName),
            'Property retrieved'
        );
        $this->assertEquals(
            $expectedPropertyExists,
            $record->propertyExists($propertyName),
            "Property existence identified"
        );
    }

    /**
     * Provides record data for testing setPropertyValue().
     *
     * @return array
     */
    public function setPropertyValueDataProvider() : array
    {
        $basicData = [
            'string_property' => 'value1',
            'int_property' => 0,
            'null_property' => null,
        ];
        $data['overwrite existing property'] = [
            $basicData,        // Data for initializing a record.
            'string_property',  // Property to set.
            'value2',           // Value to set it to.
        ];
        $data['new property'] = [
            $basicData,
            'new_property',
            5,
        ];
        $data['null property'] = [
            [],
            'null_property',
            null,
        ];
        return $data;
    }

    /**
     * Test setPropertyValue().
     *
     * @dataProvider setPropertyValueDataProvider
     *
     * @param array $data
     *   The data array for initially constructing the record.
     * @param string $propertyName
     *   Name of property to set.
     * @param mixed $propertyValue
     *   Value to set the property (and to expect to be retrieved).
     * @param bool $expectedPropertyExists
     *   Expected result from propertyExists($propertyName).
     */
    public function testSetPropertyValue(array $data, string $propertyName, $propertyValue)
    {
        /** @var RecordFactory $factory */
        $factory = new $this->recordFactoryClass();
        $record = $factory->create($data);
        $record->setPropertyValue($propertyName, $propertyValue);
        $this->assertEquals(
            $propertyValue,
            $record->getPropertyValue($propertyName),
            'Property retrieved'
        );
        $this->assertEquals(
            true,
            $record->propertyExists($propertyName),
            "Property existence identified"
        );
    }
}
