<?php
declare(strict_types=1);

namespace Soong\Tests\Contracts\Extractor;

use Soong\Contracts\Data\Record;
use Soong\Contracts\Filter\Filter;

/**
 * Simple filter to use in tests.
 *
 * One configuration option is supported, 'criteria', which consists of an array
 * containing two values: the name of the property whose value will be checked,
 * and the value to compare it to. Some examples:
 *
 * @code
 * // This filter accepts only data records whose id property is equal to 5.
 * $filter = new Select(['criteria' => ['id', 5]]);
 * @endcode
 */
class TestFilter implements Filter
{
    protected $configuration = [];

    public function __construct(array $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * Retrieve a configuration value for a named option.
     *
     * @param string $optionName
     *   Name of the option to retrieve.
     *
     * @return mixed|null
     *   Retrieved value, or NULL if unset.
     */
    public function getConfigurationValue(string $optionName)
    {
        return $this->configuration[$optionName];
    }

    /**
     * Retrieve a list of all available configuration values.
     *
     * @return iterable
     *   Option values keyed by names.
     */
    public function getAllConfigurationValues() : iterable
    {
        return $this->configuration;
    }

    /**
     * @inheritdoc
     */
    public function filter(Record $record): bool
    {
        $criteria = $this->getConfigurationValue('criteria');
        [$propertyName, $testValue] = $criteria;
        $propertyValue = $record->getPropertyValue($propertyName);
        return $propertyValue == $testValue;
    }
}
