<?php

namespace Soong\Tests\Contracts\Extractor;

use Psr\SimpleCache\CacheInterface;

/**
 * Base class for testing CountableExtractor implementations.
 *
 * Adds count() testing to ExtractorTestBase.
 */
abstract class CountableExtractorTestBase extends ExtractorTestBase
{

    /**
     * Test count() with no caching.
     *
     * @dataProvider extractAllDataProvider
     *
     * @param array $configuration
     *   Extractor configuration.
     * @param mixed $expected
     *   Expected set of data records returned.
     */
    public function testCount(array $configuration, $expected)
    {
        $extractor = new $this->extractorClass($configuration);
        $this->assertEquals(count($expected), $extractor->count());
    }

    /**
     * Test count() with caching.
     *
     * @dataProvider extractAllDataProvider
     *
     * @param array $configuration
     *   Extractor configuration.
     * @param mixed $expected
     *   Expected set of data records returned.
     */
    public function testCountCache(array $configuration, $expected)
    {
        $expectedCount = count($expected);
        $cacheKey = 'test_key';
        $configuration['cache_count'] = true;
        $configuration['cache_key'] = $cacheKey;
        $cache = $this->createStub(CacheInterface::class);
        $cache->method('get')
            ->will($this->onConsecutiveCalls(null, $expectedCount));
        $configuration['cache'] = $cache;
        $extractor = new $this->extractorClass($configuration);
        $this->assertEquals($expectedCount, $extractor->count());
        $this->assertEquals($expectedCount, $extractor->count());
    }
}
