<?php

namespace Soong\Tests\Contracts\Loader;

use PHPUnit\Framework\TestCase;
use Soong\Contracts\Loader\Loader;

/**
 * Base class for testing Loader implementations.
 *
 * To test a loader class, extend this class and implement setUp(),
 * assigning the fully-qualified class name to loaderClass:
 *
 * @code
 *     protected function setUp() : void
 *     {
 *         $this->loaderClass = '\Soong\Loader\Csv';
 *     }
 * @endcode
 *
 * And implement a data provider, where each row of data provided contains a
 * configuration array for the loader, a list of all properties, and a list of
 * the key properties:
 *
 * @code
 *     public function propertyDataProvider()
 *     {
 *         $properties1 = [
 *             'field1',
 *             'field2',
 *             ...
 *         ];
 *         $key_properties1 = [
 *                 'field1' => ['type' => 'string'],
 *         ],
 *         $configuration1 = [
 *             'key_properties' => $key_properties1,
 *         ];
 *         return [
 *             'set one' => [$configuration1, $properties1, $key_properties1],
 *             ...
 *         ];
 *     }
 * @endcode
 */
abstract class LoaderTestBase extends TestCase
{

    /**
     * Fully-qualified name of a Loader implementation.
     *
     * @var Loader $loaderClass
     */
    protected $loaderClass;

    /**
     * Test getProperties() and getKeyProperties().
     *
     * @dataProvider propertyDataProvider
     *
     * @param array $configuration
     *   Extractor configuration.
     * @param array $expectedProperties
     *   Expected set of property metadata returned.
     * @param array $expectedKeyProperties
     *   Expected set of key property metadata returned.
     */
    public function testGetProperties(array $configuration, array $expectedProperties, array $expectedKeyProperties)
    {
        $loader = new $this->loaderClass($configuration);
        $this->assertEquals($expectedProperties, $loader->getProperties());
        $this->assertEquals($expectedKeyProperties, $loader->getKeyProperties());
    }
}
