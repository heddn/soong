<?php
declare(strict_types=1);

namespace Soong\Tests\Configuration;

use PHPUnit\Framework\TestCase;

/**
 * Tests the \Soong\Configuration\OptionsResolverComponent class.
 */
class OptionsResolverComponentTest extends TestCase
{

    /**
     * Provides configuration to test.
     *
     * @return array
     */
    public function configurableComponentDataProvider() : array
    {
        // @todo Lots more possibilities to test.
        $data['required option present, no default'] = [
            ['option1' => 'foo'],   // Options to set.
            'option1',              // Name of option to retrieve.
            'foo',                  // Expected value retrieved.
            null,                   // Expected exception class.
        ];
        $data['required option missing, no default'] = [
            [],
            'option1',
            null,
            'Symfony\Component\OptionsResolver\Exception\MissingOptionsException',
        ];
        $data['required option present, with default'] = [
            ['option1' => 'foo', 'option2' => 'bar'],
            'option2',
            'bar',
            null,
        ];
        $data['required option missing, with default'] = [
            ['option1' => 'foo'],
            'option2',
            'default bar',
            null,
        ];
        $data['with incrementing normalizer'] = [
            ['option1' => 'foo', 'option3' => 41],
            'option3',
            42,
            null,
        ];
        $data['with legal allowed value'] = [
             ['option1' => 'foo', 'option4' => -12],
            'option4',
            -12,
            null,
        ];
        $data['with disallowed value'] = [
            ['option1' => 'foo', 'option4' => 12],
            'option4',
            null,
            'Symfony\Component\OptionsResolver\Exception\InvalidOptionsException',
        ];
        return $data;
    }

    /**
     * Test getConfigurationValue().
     *
     * @dataProvider configurableComponentDataProvider
     *
     * @param array $providedOptions
     *   Options to set.
     * @param string $optionToRetrieve
     *   Option whose value we're checking.
     * @param mixed $expectedValue
     *   Option value we should receive.
     * @param string $exceptionClass
     *   Name of expected exception class, if any.
     */
    public function testGetConfigurationValue(
        array $providedOptions,
        string $optionToRetrieve,
        $expectedValue,
        string $exceptionClass = null) : void
    {
        if (!is_null($exceptionClass)) {
            $this->expectException($exceptionClass);
        }
        $component = new SampleOptionsResolverComponent($providedOptions);
        $result = $component->getConfigurationValue($optionToRetrieve);
        $this->assertEquals($expectedValue, $result, 'Option value retrieved');
    }

    /**
     * Test getAllConfigurationValues().
     *
     * @dataProvider configurableComponentDataProvider
     *
     * @param string $optionName
     *   Option to set.
     * @param mixed $initialValue
     *   The data array for initially constructing the record.
     * @param mixed $expectedValue
     *   Name of property to test.
     */
    public function testGetAllConfigurationValues() : void
    {
        $component = new SampleOptionsResolverComponent(['option1' => 'foo']);
        $result = $component->getAllConfigurationValues();
        $expectedResults = [
            'option1' => 'foo',
            'option2' => 'default bar',
        ];
        $this->assertEquals($expectedResults, $result, 'All option values retrieved');
    }
}
