<?php
declare(strict_types=1);

namespace Soong\Tests\Configuration;

use Soong\Configuration\OptionsResolverComponent;
use Symfony\Component\OptionsResolver\Options;

class SampleOptionsResolverComponent extends OptionsResolverComponent
{

    /**
     * {@inheritDoc}
     */
    protected function optionDefinitions(): array
    {
        $options['option1'] = [
            'required' => true,
            'allowed_types' => 'string',
        ];
        $options['option2'] = [
            'required' => true,
            'default_value' => 'default bar',
            'allowed_types' => 'string',
        ];
        $options['option3'] = [
            'required' => false,
            'allowed_types' => 'int',
            'normalizer' => function (Options $options, $sourceValue) {
                return ++$sourceValue;
            },
        ];
        $options['option4'] = [
            'required' => false,
            'allowed_types' => 'int',
            'allowed_values' => [51, 89, -12],
        ];
        return $options;
    }
}
