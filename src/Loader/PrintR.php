<?php
declare(strict_types=1);

namespace Soong\Loader;

use Soong\Contracts\Data\RecordPayload;

/**
 * Loader for testing/debugging pipelines.
 */
class PrintR extends LoaderBase
{

    /**
     * @inheritdoc
     */
    public function __invoke(RecordPayload $payload) : RecordPayload
    {
        print_r($payload->getDestinationRecord());
        return $payload;
    }

    /**
     * @inheritdoc
     */
    public function getKeyProperties(): array
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getProperties(): array
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function delete(array $key) : void
    {
        // @todo not supported
    }
}
