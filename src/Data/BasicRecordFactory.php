<?php
declare(strict_types=1);

namespace Soong\Data;

use Soong\Contracts\Data\Record;
use Soong\Contracts\Data\RecordFactory;

/**
 * Basic implementation of a Record factory.
 */
class BasicRecordFactory implements RecordFactory
{

    /**
     * @inheritdoc
     */
    public function create(array $data = []): Record
    {
        return new BasicRecord($data);
    }
}
