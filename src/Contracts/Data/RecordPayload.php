<?php
declare(strict_types=1);

namespace Soong\Contracts\Data;

/**
 * Collection of named data properties and values.
 */
interface RecordPayload
{

    /**
     * Retrieves the migration source record.
     *
     * @return \Soong\Contracts\Data\Record
     */
    public function getSourceRecord() : Record;

    /**
     * Retrieves the migration destination record.
     *
     * @return \Soong\Contracts\Data\Record
     */
    public function getDestinationRecord() : Record;

    /**
     * Replaces the migration destination record.
     *
     * @param \Soong\Contracts\Data\Record $record
     */
    public function setDestinationRecord(Record $record) : void;
}
