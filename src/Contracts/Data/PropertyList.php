<?php
declare(strict_types=1);

namespace Soong\Contracts\Data;

/**
 * Collection of available data properties.
 */
interface PropertyList
{

    /**
     * List the properties available in records generated by this extractor.
     *
     * @return array
     *   Array of property names.
     */
    public function getProperties() : array;

    /**
     * List the properties which form a unique key for the extracted data.
     *
     * @return array
     *   Array keyed by property name, with value being an array containing
     *   a 'type' key whose value is the type of the property.
     */
    public function getKeyProperties() : array;
}
