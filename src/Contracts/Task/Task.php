<?php
declare(strict_types=1);

namespace Soong\Contracts\Task;

use Soong\Contracts\Configuration\ConfigurableComponent;

/**
 * Interface for task definitions.
 */
interface Task extends ConfigurableComponent
{

}
