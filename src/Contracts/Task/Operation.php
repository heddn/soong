<?php
declare(strict_types=1);

namespace Soong\Contracts\Task;

use Soong\Task\TaskPayload;

/**
 * Interface for operations on tasks. Note we do not extend
 * \League\Pipeline\StageInterface, so we can typehint __invoke().
 */
interface Operation
{
    /**
     * Process the payload.
     *
     * @param \Soong\Task\TaskPayload $payload
     *
     * @return \Soong\Task\TaskPayload
     */
    public function __invoke(TaskPayload $payload) : TaskPayload;
}
