<?php
declare(strict_types=1);

namespace Soong\Contracts\Loader;

use Soong\Contracts\Configuration\ConfigurableComponent;
use Soong\Contracts\Data\PropertyList;
use Soong\Contracts\Data\RecordPayload;

/**
 * Loaders take one Record at a time and load them into a destination.
 */
interface Loader extends ConfigurableComponent, PropertyList
{

    /**
     * This needs to return disposition (success, failure) and key of result.
     *
     * @param RecordPayload $payload
     *   Payload containing data to be loaded into the destination.
     *
     * @throws \Soong\Contracts\Exception\LoaderException
     */
    public function __invoke(RecordPayload $payload) : RecordPayload;

    /**
     * Remove a record which has been loaded from the destination.
     *
     * @param array $key
     *   Unique key of the destination record to be removed.
     *
     * @throws \Soong\Contracts\Exception\LoaderException
     */
    public function delete(array $key) : void;
}
