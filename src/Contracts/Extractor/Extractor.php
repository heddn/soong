<?php
declare(strict_types=1);

namespace Soong\Contracts\Extractor;

use Soong\Contracts\Configuration\ConfigurableComponent;
use Soong\Contracts\Data\PropertyList;
use Soong\Contracts\Data\Record;

/**
 * Extractors turn a data source into a series of Records.
 */
interface Extractor extends ConfigurableComponent, PropertyList
{

    /**
     * Iterator taking into account any applicable filtering.
     *
     * @return Record[]
     *   One data record from the source being extracted.
     */
    public function extractFiltered() : iterable;

    /**
     * Iterator over all canonical source data.
     *
     * Note filtering may be applied here to the raw data, if there are records
     * in the raw source which are not part of the logical source for this
     * extraction process.
     *
     * @return Record[]
     *   One data record from the source being extracted.
     *
     * @throws \Soong\Contracts\Exception\ExtractorException
     */
    public function extractAll() : iterable;
}
