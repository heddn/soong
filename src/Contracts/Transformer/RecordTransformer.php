<?php
declare(strict_types=1);

namespace Soong\Contracts\Transformer;

use Soong\Contracts\Configuration\ConfigurableComponent;
use Soong\Contracts\Data\RecordPayload;

/**
 * Accept a data record and turn it into another data record.
 */
interface RecordTransformer extends ConfigurableComponent
{

    /**
     * Accept a data record and turn it into another data record.
     *
     * @param RecordPayload $payload
     *   Payload containing source and destination records.
     *
     * @return \Soong\Contracts\Data\RecordPayload
     *   Payload with modified destination record.
     *
     * @throws \Soong\Contracts\Exception\RecordTransformerException
     */
    public function __invoke(RecordPayload $payload) : RecordPayload;
}
