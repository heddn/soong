<?php
declare(strict_types=1);

namespace Soong\Transformer\Record;

use League\Pipeline\Pipeline;
use Soong\Configuration\OptionsResolverComponent;
use Soong\Contracts\Data\RecordPayload;
use Soong\Contracts\Transformer\RecordTransformer;
use Symfony\Component\OptionsResolver\Options;

/**
 * Populates records property-by-property.
 */
class PropertyMapper extends OptionsResolverComponent implements RecordTransformer
{

    /**
     * @inheritdoc
     */
    protected function optionDefinitions(): array
    {
        $options = parent::optionDefinitions();
        $options['property_map'] = [
            'required' => true,
            'allowed_types' => 'array',
            'normalizer' => function (Options $options, $propertyMap) {
                $pipelines = [];
                // Build a pipeline for each mapped result property.
                foreach ($propertyMap as $resultProperty => $transformerList) {
                    $pipelines[$resultProperty]['pipeline'] = new Pipeline();
                    $pipelines[$resultProperty]['source_property'] = null;
                    foreach ($transformerList as $transformerEntry) {
                        $transformer = $transformerEntry['transformer'];
                        if (isset($transformerEntry['source_property'])) {
                            $pipelines[$resultProperty]['source_property'] = $transformerEntry['source_property'];
                        }
                        $pipelines[$resultProperty]['pipeline'] =
                            $pipelines[$resultProperty]['pipeline']->pipe($transformer);
                    }
                }
                return $pipelines;
            }
        ];
        return $options;
    }

    /**
     * @inheritdoc
     */
    public function __invoke(RecordPayload $recordPayload): RecordPayload
    {
        $pipelines = $this->getConfigurationValue('property_map');
        $sourceRecord = $recordPayload->getSourceRecord();
        $destinationRecord = clone $recordPayload->getDestinationRecord();
        foreach ($pipelines as $resultProperty => $pipelineEntry) {
            $propertyPayload = $pipelineEntry['source_property'] ?
                $sourceRecord->getPropertyValue($pipelineEntry['source_property']) : null ;
            $destinationRecord->setPropertyValue(
                $resultProperty,
                $pipelineEntry['pipeline']->process($propertyPayload)
            );
        }
        $recordPayload->setDestinationRecord($destinationRecord);
        return $recordPayload;
    }
}
