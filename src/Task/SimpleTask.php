<?php
declare(strict_types=1);

namespace Soong\Task;

use Soong\Configuration\OptionsResolverComponent;
use Soong\Contracts\Task\Task;
use Soong\Contracts\Task\TaskContainer;

/**
 * Basic base class for migration tasks.
 */
class SimpleTask extends OptionsResolverComponent implements Task
{

    /**
     * @inheritdoc
     */
    protected function optionDefinitions(): array
    {
        $options = parent::optionDefinitions();
        $options['container'] = [
            'required' => true,
            'allowed_types' => TaskContainer::class,
        ];
        return $options;
    }
}
