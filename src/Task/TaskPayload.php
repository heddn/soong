<?php
declare(strict_types=1);

namespace Soong\Task;

/**
 * Information to be passed through the task pipeline.
 */
class TaskPayload
{

    /**
     * @var array
     */
    protected $options = [];

    public function __construct(array $options)
    {
        $this->options = $options;
    }

    /**
     * List of runtime options to apply.
     *
     * @return array
     */
    public function getOptions() : array
    {
        return $this->options;
    }
}
