<?php
declare(strict_types=1);

namespace Soong\Task;

use League\Pipeline\Pipeline;
use Soong\Data\BasicRecordPayload;

class MigrateOperation extends TaskOperation
{

    /**
     * @inheritDoc
     */
    public function __invoke(TaskPayload $taskPayload): TaskPayload
    {
        $task = $this->task;
        if (empty($task->getExtractor())) {
            // @todo Throw a TaskException.
            return $taskPayload;
        }

        $processedCount = 0;
        $limit = $taskPayload->getOptions()['limit'] ?? 0;

        /** @var \Soong\Contracts\Data\RecordFactory $recordFactory */
        $recordFactory = $task->getConfigurationValue('record_factory');

        foreach ($task->getExtractor()->extractFiltered() as $sourceData) {
            $destinationData = $recordFactory->create();
            $pipeline = new Pipeline();
            /** @var \Soong\Contracts\Transformer\RecordTransformer $recordTransformer */
            foreach ($task->getConfigurationValue('transform') as $recordTransformer) {
                $pipeline = $pipeline->pipe($recordTransformer);
            }
            $pipeline = $pipeline->pipe($task->getLoader());
            /** @var \Soong\Contracts\Data\RecordPayload $payload */
            $recordPayload = $pipeline->process(new BasicRecordPayload($sourceData, $destinationData));
            if (!is_null($task->getKeyMap())) {
                // @todo Handle multi-column keys.
                $extractedKey = $recordPayload->getSourceRecord()
                    ->getPropertyValue(array_keys($task->getExtractor()->getKeyProperties())[0]);
                $loadedKey = $recordPayload->getDestinationRecord()
                    ->getPropertyValue(array_keys($task->getLoader()->getKeyProperties())[0]);
                $task->getKeyMap()->saveKeyMap([$extractedKey], [$loadedKey]);
            }
            $processedCount++;
            if (($limit > 0) && $processedCount >= $limit) {
                return $taskPayload;
            }
        }
        return $taskPayload;
    }
}
